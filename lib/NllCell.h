#ifndef NLL_CELL_H
#define NLL_CELL_H

#include <iostream>
#include <vector>

struct NllCell
{
    double poi;
    double nll;
};

static bool CompNll(const NllCell &cell1, const NllCell &cell2) {return cell1.nll < cell2.nll;}

static bool CompPoi(const NllCell &cell1, const NllCell &cell2) {return cell1.poi < cell2.poi;}

#endif
