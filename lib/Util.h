#ifndef UTIL_H
#define UTIL_H

//...................................//
//C/C++ STL
#include <cstdlib>
#include <iostream>
#include <vector>

//...................................//
//ROOT
#include "RtypesCore.h"
#include "TString.h"
#include "TSpline.h"

//...................................//
//Nll Scan

//Line style for graphs
const std::vector<Style_t> styles = {kSolid, kSolid, kSolid, kDashed, kSolid, kSolid, kSolid, kSolid};
const std::vector<Color_t> colors = {kBlack, kBlue, kRed, kBlack, kGreen + 2, kMagenta, kOrange + 7, kViolet + 1};

//Transfer TString into vector<TString> by seperator
std::vector<TString> SplitString(const TString& theOpt, const char separator);

//Binary search for error of nll, declaration must be defination for function with default parameter
double BinarySearch(TSpline *s, double target, double xmin, double xmax, double tolerance = 1e-3)
{
    if(xmin < s->GetXmin())
    {
        std::cout << "Warning: Input min poi out of range. Use the range of the input spline: "
             << s->GetXmin() << " to " << xmax         << std::endl;
        xmin = s->GetXmin();
    }
    if(xmax>s->GetXmax()){
        std::cout << "Warning: Input max poi out of range. Use the range of the input spline: "
             << xmin         << " to " << s->GetXmax() << std::endl;
        xmax = s->GetXmax();
    }
    if(xmin > xmax)
    {
        std::cerr << "Error: Wrong search range, xmin > xmax." << std::endl;
        std::cerr << "xmin: " << xmin << ", xmax: " << xmax << std::endl;
        abort();
    }

    double begin = xmin;
    double end = xmax;
    double median=(end-begin)/2;
    double thisUL=s->Eval(begin);
    int counter = 0;
    while(fabs(thisUL - target) > tolerance && counter < 10000)
    {
        median = (end + begin)/2;
        thisUL = s->Eval(median);
        if((thisUL - target)*(s->Eval(end) - target) < 0)        begin = median;
        else if((thisUL - target)*(s->Eval(begin) - target) < 0) end   = median;

        ++counter;
    }

    return median;
}


#endif
