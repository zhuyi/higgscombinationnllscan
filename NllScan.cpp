//...................................//
//C/C++ STL
#include <cstdlib>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <memory>

//...................................//
//ROOT
#include "TFile.h"
#include "TString.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TSpline.h"
#include "TMath.h"
#include "RootStatsHead.h"

//...................................//
//Nll Scan
#include "Util.h"
#include "NllCell.h"

int main(int argc, char* argv[])
{
//...................................//
//Input parameter
//...................................//
//Read parameters from command line
    TString jobname = argv[1];
    TString input   = argv[2];
    TString leg     = argv[3];
    TString poiName = argv[4];
    TString lumi    = argv[8];  //Used in TPaveText
    TString option  = argv[9];
    TString label   = argv[10];

    double inmin = atof(argv[5]);
    double inmax = atof(argv[6]);
    double inmaxy = atof(argv[7]);

//Create input list by seperator ',' from input parameter 'input' 
    std::vector<TString> inputList(SplitString(input, ','));
    std::cout << "Input files: " << std::endl;
    for(auto input : inputList) std::cout << input << std::endl;

//Create legend list by seperator ',' from input parameter 'leg'
    std::vector<TString> leglist(SplitString(leg, ','));

//...................................//
//Declaration
//...................................//
//Declare outstream log file
    //std::ofstream fout("fig/nllscan/" + jobname + "/nllscan_" + poiName + ".txt", std::ios::out);

//Declare output .root file to save graphs
    system("mkdir -vp fig/nllscan/" + jobname);
    TString outputCanvas = "fig/nllscan/" + jobname + "/nllscan_" + jobname;
    TFile *fhist = TFile::Open(Form("%s.root", outputCanvas.Data()), "recreate");

//Declare graphs
    TCanvas *c = new TCanvas("c", "", 1650, 1400);
    TMultiGraph *mg = new TMultiGraph("mg", "");
    std::vector<TGraph*>   graphs;

//Declara TSpline5 for error finding
    std::vector<TSpline5*> splines;

//...................................//
//Execute input files
//...................................//
    for(int ifile = 0; ifile < inputList.size(); ifile++)
    {
        std::cout << "Executing input file: " << inputList.at(ifile) << std::endl;
        const std::unique_ptr<TChain> tree = std::make_unique<TChain>("nllscan");
        tree->Add(inputList.at(ifile));

//Register leaves
        int status = -999.;
        double poi = -999.;
        double nll = -999.;
        tree->SetBranchAddress("status", &status);
        tree->SetBranchAddress(poiName, &poi);
        tree->SetBranchAddress("nll", &nll);

//Loop events
        double lPoi = 999999.;
        double rPoi = -999999.;
        double zeroNll = -999999.;
        std::vector<NllCell> cells;
        for(int ievt = 0; ievt < tree->GetEntries(); ievt++)
        {
            tree->GetEntry(ievt);

            if(!std::isnan(nll) && (poi >= inmin) && (poi <= inmax))
            {
                if(poi < lPoi) lPoi = poi;
                if(poi > rPoi) rPoi = poi;
                if(poi == 0.)  zeroNll = nll;

                NllCell tempcell = {poi, nll};
                cells.push_back(tempcell);
            }
        }

//Get minimum/maximum of nll, recalculate nll by nll - min nll
        auto minNllIt = std::min_element(cells.begin(), cells.end(), CompNll);
        double minNll = (*minNllIt).nll;
        double minPoi = (*minNllIt).poi;
        for(auto &cell : cells) cell.nll = 2.*(cell.nll - minNll);

        auto maxNllIt = std::min_element(cells.begin(), cells.end(), CompNll);
        double maxNll = (*maxNllIt).nll;

//Sort poi-nll by poi
        std::sort(cells.begin(), cells.end(), CompPoi);

//Assign to vectors and print out
        const int len = cells.size();
        double poiArr[len];
        double nllArr[len];
        for(int icell = 0; icell < len; icell++)
        {
            //std::cout << poiName << ": " << cells.at(icell).poi
            //          << "	nll: "     << cells.at(icell).nll << std::endl;
            poiArr[icell] = cells.at(icell).poi;
            nllArr[icell] = cells.at(icell).nll;
        }

//Fill and set graphs
        graphs.push_back(new TGraph(len, poiArr, nllArr));
        graphs.at(ifile)->SetLineWidth(2);
        graphs.at(ifile)->SetLineColor(colors.at(ifile));
        graphs.at(ifile)->SetLineStyle(styles.at(ifile));
        graphs.at(ifile)->SetMarkerStyle(7);
        graphs.at(ifile)->SetMarkerColor(colors.at(ifile));

        c->cd();
        graphs.at(ifile)->Draw("C same");

//Fill TSpline5 and interpolate the graph
        splines.push_back(new TSpline5(Form("gr_%d", ifile), graphs.at(ifile)));
        splines.at(ifile)->SetLineColor(colors.at(ifile));
        splines.at(ifile)->SetLineWidth(2);

//Calculate error from TSpline5
        double clnumber = 1.;
        if(option.Contains("2sigma")) clnumber = 4.;
        if(option.Contains("95CL"))   clnumber = 3.84;

        double errLo = 1., errHi = 1.;
        errLo = BinarySearch(splines.at(ifile), clnumber, lPoi,   minPoi, 1e-8);
        errHi = BinarySearch(splines.at(ifile), clnumber, minPoi, rPoi,   1e-8);

//Print results to screen
        std::cout << "Scan result: "
                  << minPoi
                  << " - "    << errHi  - minPoi
                  << " + "    << minPoi - errLo
                  << std::endl;

//Print results to log file
/*
        fout << std::endl;
        fout << "-------------------------------------------------" << std::endl;
        fout << "Result from " << leglist.at(ifile) << std::endl;
        fout << "**** " << "#Mu" << " best fit: "   << minPoi
                                 << " - "           << errHi - minPoi
                                 << " + "           << minPoi - errLo
             << std::endl;
        fout << std::setprecision(10) << " nll at POI = 0: " << zeroNll << ", min nll: " << minNll
             << std::endl;

        double pvalueZero = 1 - ROOT::Math::normal_cdf(sqrt(2*(zeroNll - minNll)));
        fout << std::endl;
        fout << std::setprecision(10) << "Consistency with B-only: " << pvalueZero
                                      << " " << sqrt(2*(zeroNll - minNll)) << "\\sigma"
             << std::endl;
        fout << std::setprecision(10) << "Consistency with B-only: " << pvalueZero
                                      << " " << RooStats::PValueToSignificance(pvalueZero) << "\\sigma"
             << std::endl;
*/
    }

//...................................//
//Write to .root output
//...................................//
//Write each TGraph
    c->cd();
    for(int igraph = 0; igraph < graphs.size(); igraph++)
        graphs.at(igraph)->Write(leglist.at(igraph));

//Write TMultiGraph
    for(auto graph : graphs) mg->Add(graph);
    mg->Write();

//Write TPaveText
    

//Write TCanvas
    c->Write();

//Show and save
    fhist->ls();
    fhist->Close();

    return 0;
}
