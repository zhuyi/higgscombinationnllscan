//...................................//
//C/C++ STL
#include <stdlib.h>
#include <iostream>
#include <vector>

//...................................//
//ROOT
#include "TString.h"

//...................................//
//Nll Scan
#include "Util.h"

//Transfer TString into vector<TString> by seperator
std::vector<TString> SplitString(const TString& theOpt, const char separator)
{
    std::vector<TString> splitV;

    TString splitOpt(theOpt);
    splitOpt.ReplaceAll("\n", " ");
    splitOpt = splitOpt.Strip(TString::kBoth, separator); //Split splitOpt into TSubStrings by seperator
    while(splitOpt.Length() > 0)
    {
        if(!splitOpt.Contains(separator))
        {
            splitV.push_back(splitOpt);
            break;
        }
        else
        {
            //If the TSubString contains the seperator, fill the splitV from 1st char to 1st seperator
            TString toSave = splitOpt(0, splitOpt.First(separator));
            splitV.push_back(toSave);
            splitOpt = splitOpt(splitOpt.First(separator), splitOpt.Length()); //Discard the chars added into the vector
        }
        splitOpt = splitOpt.Strip(TString::kLeading, separator);
    }

    return splitV;
}
